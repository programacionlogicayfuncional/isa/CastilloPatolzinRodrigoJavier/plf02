(ns plf02.core)

(defn funcion-associative?-1
  [x]
  (associative? [x]))

(defn funcion-associative?-2
  [x]
  (associative? [x]))

(defn funcion-associative?-3
  [x s]
  (associative? [x s]))

(funcion-associative?-1 [1 2 3])
(funcion-associative?-2 #{2 3 4 5})
(funcion-associative?-3 [2 3] [4 5])


(defn funcion-boolean?-1
  [a]
  (boolean? a))

(defn funcion-boolean?-2
  [a]
  (boolean? a))

(defn funcion-boolean?-3
  [a]
  (boolean? (even? a)))

(funcion-boolean?-1 true)
(funcion-boolean?-2 false)
(funcion-boolean?-3 5)


(defn funcion-char?-1
  [a]
  (char? a))

(defn funcion-char?-2
  [a]
  (char? a))

(defn funcion-char?-3
  [a]
  (char? a))

(funcion-char?-1 \a)
(funcion-char?-2 "a")
(funcion-char?-3 22)

(defn funcion-coll?-1
  [a]
  (coll? a))

(defn funcion-coll?-2
  [a]
  (coll? a))

(defn funcion-coll?-3
  [a]
  (coll? a))

(funcion-coll?-1 [1 2 3 4])
(funcion-coll?-2 {2 3 4 5})
(funcion-coll?-3 "hola")

(defn funcion-decimal?-1
  [a]
  (decimal? a))

(defn funcion-decimal?-2
  [a]
  (decimal? a))

(defn funcion-decimal?-3
  [a]
  (decimal? a))

(funcion-decimal?-1 1.0)
(funcion-decimal?-2 2/3)
(funcion-decimal?-3 10.7548M)

(defn funcion-double?-1
  [x]
  (double? x))

(defn funcion-double?-2
  [x]
  (double? x))

(defn funcion-double?-3
  [x]
  (double? x))

(funcion-double?-1 1.483)
(funcion-double?-2 1M)
(funcion-double?-3 3.151492653589793238462)

(defn funcion-float?-1
  [a]
  (float? a))

(defn funcion-float?-2
  [a]
  (float? a))

(defn funcion-float?-3
  [a]
  (float? a))

(funcion-float?-1 3.0)
(funcion-float?-2 100.65)
(funcion-float?-3 79.23M)

(defn funcion-ident?-1
  [a]
  (ident? a))
(defn funcion-ident?-2
  [a]
  (ident? a))
(defn funcion-ident?-3
  [a]
  (ident? a))

(funcion-ident?-1 :a)
(funcion-ident?-2 :abchello)
(funcion-ident?-3 "hello")

(defn funcion-indexed?-1
  [a]
  (indexed? a))

(defn funcion-indexed?-2
  [a]
  (indexed? a))

(defn funcion-indexed?-3
  [a]
  (indexed? a))

(funcion-indexed?-1 [100 40 59 \a \b])
(funcion-indexed?-2 #{20 40 89 100 \a})
(funcion-indexed?-3 (list 1 2 3))

(defn funcion-int?-1
  [x]
  (int? x))

(defn funcion-int?-2
  [x]
(int? x))

(defn funcion-int?-3
  [x]
  (filter int? x))

(funcion-int?-1 (* 2 3))
(funcion-int?-2 (/ (* 2 3) (* 4 3)))
(funcion-int?-3 [1 2 3 4.1])

(defn funcion-integer?-1
  [a]
  (integer? a))
(defn funcion-integer?-2
  [a]
  (integer? a))

(defn funcion-integer?-3
  [a]
  (filter integer? a))

(funcion-integer?-1 57892)
(funcion-integer?-2 37N)
(funcion-integer?-3 [83 34 20.67 2N 49M 2/3 4/2])

(defn funcion-keyword?-1
  [a]
  (keyword? a))

(defn funcion-keyword?-2
  [a]
  (keyword? a))

(defn funcion-keyword?-3
  [a]
  (filter keyword? a))

(funcion-keyword?-1 :-o)
(funcion-keyword?-2 :////)
(funcion-keyword?-3 [:a 34 :Hello :+ \v :23])

(defn funcion-list?-1
  [a]
  (list? a))

(defn funcion-list?-2
  [a]
  (list? a))

(defn funcion-list?-3
  [a]
  (list? a))

(funcion-list?-1 '(1 2 3))
(funcion-list?-2 '(1 2 [3 2] \a "E"))
(funcion-list?-3 [2 3 18 \a '(1 2)])

(defn funcion-map-entry?-1
  [a]
  (map-entry? a))

(defn funcion-map-entry?-2
  [a]
  (map-entry? a))

(defn funcion-map-entry?-3
  [a]
  (map-entry? a))

(funcion-map-entry?-1 {:a 23 :d 48 :hola 2})
(funcion-map-entry?-2 (first {:a 1 :b 3 :c 156 :-o 0}))
(funcion-map-entry?-3 (first {:1 9 :e 39 :f 1 :/ 3}))

(defn funcion-map?-1
  [a]
  (map? a))

(defn funcion-map?-2
  [a]
  (map? a))

(defn funcion-map?-3
  [a]
  (filter map? a))

(funcion-map?-1 {:a 1 :b 3 :code 5 :key \q})
(funcion-map?-1 (hash-map :e 123 :r \c :p \p))
(funcion-map?-3 [{:a 1 :b 2} #{1 4 34} {:hola 12 :-o \s} '(1 2 3) [23 \q]])

(defn funcion-nat-int?-1
  [a]
  (nat-int? a))

(defn funcion-nat-int?-2
  [a]
  (nat-int? a))

(defn funcion-nat-int?-3
  [a]
  (filter nat-int? a))

(funcion-nat-int?-1 97538201)
(funcion-nat-int?-2 -56)
(funcion-nat-int?-3 [1 \a :d 78 -32 90 2.3])

(defn funcion-number?-1
  [a]
  (number? a))

(defn funcion-number?-2
  [a]
  (number? a))

(defn funcion-number?-3
  [a]
  (filter number? a))

(funcion-number?-1 (* 234 2))
(funcion-number?-2 "368")
(funcion-number?-3 [-38 34 90.0 :12 \a "750" -5/2])

(defn funcion-pos-int?-1
  [a]
  (pos-int? a))

(defn funcion-pos-int?-2
  [a]
  (pos-int? a))

(defn funcion-pos-int?-3
  [a]
  (map pos-int? a))

(funcion-pos-int?-1 357283)
(funcion-pos-int?-2 90N)
(funcion-pos-int?-3 [10 -43 9.8 1/2 78 3])

(defn funcion-ratio?-1
  [a]
  (ratio? a))

(defn funcion-ratio?-2
  [a]
  (filter ratio? a))

(defn funcion-ratio?-3
  [a]
  (map  ratio? a))

(funcion-ratio?-1 1/2)
(funcion-ratio?-2 [23 15/7 -3/5 -34 90 16.78])
(funcion-ratio?-3 [23 15/7 -3/5 -34 90 16.78 67/4])

(defn funcion-rational?-1
  [a]
  (rational? a))

(defn funcion-rational?-2
  [a]
  (rational? a))

(defn funcion-rational?-3
  [a]
  (map rational? a))

(funcion-rational?-1 3/5)
(funcion-rational?-2 (/ 1 2))
(funcion-rational?-3 [23 -34/2 45.90 3.1 -2 1 0])

(defn funcion-seq?-1
  [a]
  (seq? a))

(defn funcion-seq?-2
  [a]
  (seq? a))

(defn funcion-seq?-3
  [a]
  (seq? (range a)))
  
(funcion-seq?-1 '(1 2 3 4 5 6 9))
(funcion-seq?-2 [1 32 4])
(funcion-seq?-3 100)

(defn funcion-seqable?-1
  [a]
  (seqable? a))

(defn funcion-seqable?-2
  [a]
  (seqable? a))

(defn funcion-seqable?-3
  [a]
  (seqable? a))

(funcion-seqable?-1 '(1 2 3 4 5 \a))
(funcion-seqable?-2 [23 \a :d 3 :a {:a 1}])
(funcion-seqable?-3 #{30 40 2 4.0 \q})

(defn funcion-sequential?-1
  [a]
  (sequential? a))

(defn funcion-sequential?-2
  [a]
  (sequential? a))

(defn funcion-sequential?-3
  [a]
  (sequential? a))

(funcion-sequential?-1 [20 30 \a \x :d 3/2])
(funcion-sequential?-2 {:a 34 :b 30 :c 23 :d 20})
(funcion-sequential?-3 (range 5 10))

(defn funcion-set?-1
  [a]
  (set? a))

(defn funcion-set?-2
  [a]
  (set? a))

(defn funcion-set?-3
  [a]
  (map set? a))

(funcion-set?-1 #{1 4 6 8 10 7})
(funcion-set?-2 [1 3 5 8 9 11])
(funcion-set?-3 [[3 4 6] {:a 2 :b 4} #{10 20 30} '(1 2 3)])

(defn funcion-some?-1
  [a]
  (some? a))

(defn funcion-some?-2
  [a]
  (filter some? a))

(defn funcion-some?-3
  [a]
  (map some? a))

(funcion-some?-1 40)
(funcion-some?-2 [2 :a {} #{} nil \a])
(funcion-some?-3 [2 :a {} #{} nil \a :hello nil])

(defn funcion-string?-1
  [a]
  (string? a))

(defn funcion-string?-2
  [a]
  (filter string? a))

(defn funcion-string?-3
  [a]
  (map string? a))

(funcion-string?-1 "Hello")
(funcion-string?-2 ["a" \b "" 12 "12" :d ["a" "b"]])
(funcion-string?-3 [["a" "b"] :q "a" \b "" 12 "12" :d])

(defn funcion-symbol?-1
  [a]
  (symbol? a))

(defn funcion-symbol?-2
  [a]
  (symbol? a))

(defn funcion-symbol?-3
  [a]
  (map symbol? a))

(funcion-symbol?-1 '!)
(funcion-symbol?-2 :w)
(funcion-symbol?-3 [1 \r '! "hl" :r 'we3])

(defn funcion-vector?-1
  [a]
  (vector? a))

(defn funcion-vector?-2
  [a]
  (vector? (first a)))

(defn funcion-vector?-3
  [a]
  (map vector? a))

(funcion-vector?-1 [3 \q :D 4/5])
(funcion-vector?-2 {:a 34 :b 30 :c 90})
(funcion-vector?-3 ['(1 2 3) {:q 23 :d 1} [1 2 3] #{20 30 40}])

(defn funcion-drop-1
  [a xs]
  (drop a xs))

(defn funcion-drop-2
  [a xs]
  (drop a xs))

(defn funcion-drop-3
  [a xs]
  (drop a xs))

(funcion-drop-1 2 [1 2 3 4 5])
(funcion-drop-2 -1 [1 2 3 4 5])
(funcion-drop-3 5 [\q 2 :a 4 5])